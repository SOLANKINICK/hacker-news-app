import React, {Component} from 'react';
import Header from './Components/Header.js'
import {getStoryId, getStory} from './Services/hnApi'
import Storycontainer from './Components/Storycontainer.js';

export class App extends Component{
    constructor(props){
        super(props);
        this.state={
            search : "",
            storyArray: [],
        }
    }

    componentDidMount(){
        getStoryId().then((storyId)=>{
            // console.log(storyId);
            storyId.forEach((id)=>{
                getStory(id).then((story)=>{
                    //console.log(story);
                    if(story && story.url && story.id)
                    {
                        let tempArray = [...this.state.storyArray]
                        tempArray.push(story);
                        this.setState({storyArray: tempArray});
                        console.log(this.state.storyArray);
                    }
                })
            })
        })
    }

    InputClick(event){
        const stories = [...this.state.storyArray];
        const filteredStories = stories.filter((story)=>{
            const url = story.url.toLowerCase();
            const title = story.title.toLowerCase();
            const author = story.by.toLowerCase();
            const searchInput = event.target.value; 
            if(title.indexOf(searchInput)!== -1){
                return story
            }
            else if(url.indexOf(searchInput )!== -1){
                return story;
            }
            else if(author.indexOf(searchInput) !== -1){
                return story;
            }
        })

        this.setState({storyArray : filteredStories});
    }



    render(){
        return (
            <div>
                <Header value={this.state.search} onChange = {(event)=> this.InputClick(event)}/>
                {this.state.storyArray.map((ele)=>{
                    return (
                        <Storycontainer 
                            key={ele.id}
                            data={ele}
                        />
                    );
                })}
                
            </div>
        )
    }
}