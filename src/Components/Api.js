import React, {Component} from 'react';


const baseUrl = "https://hacker-news.firebaseio.com/v0/";
const newStoryUrl = `${baseUrl}newstories.json`;
const storyUrl = `${baseUrl}item`;

export const getStoryId = ()=>{
    const result = fetch(newStoryUrl).then(data=>data.json);
    return result;
}

export const getstory = (id)=>{
    const result = fetch(`${storyUrl+id}.json`).then(story=>story.json);
    return result;
}

