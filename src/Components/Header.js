import React from 'react';

const Header = (props)=>(

    <nav class="navbar navbar-light justify-content-center" style={{backgroundColor: '#e3f2fd'}}>
    <a class="navbar-brand">Hacker News Search</a>
    <form class="form-inline">
    <input class="form-control mr-sm-2 searchInput" type="search"
      placeholder="Search" aria-label="Search" 
      value={props.search} onChange={props.onChange}/>
    {/* <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> */}
    </form>
    </nav>
 
      //   <header className="searchHeader container">
      //   <div className="searchHeader_search">
      //       <a className="searchHeader_logo" href="/">
      //         <img src="../logo.png" alt="logo" />
      //         <div className="searchHeader_label">
      //           search
      //           <br/>
      //           Heacker News
      //         </div>
      //       </a>
      //       <div className="searchHeader_container">
      //         <span className="searchIcon fa fa-search "></span>
      //         <input className="searcInput" type="search"  
      //           placeholder="search stories by title,url or author" value="this.props.search" />
              
      //       </div>
      //       <div className="searchHeader_setting">
      //         <a href="/">
      //           <i className="fa fa-gear gearImg"></i>
      //           <span>Settings</span>
      //         </a>
      //       </div>
      //     </div>
      // </header>
  
);

export default Header