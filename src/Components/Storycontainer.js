
import React, { Component } from 'react';
class Storycontainer extends Component {
    state = {  }
    render() { 
        return ( 
            <div className="card d-flex">
                
                <a href={this.props.data.url}>{this.props.data.title}</a>
                <p className="d-flex flex-row">
                    <span className="specification">{this.props.data.score}points,</span>
                    <span className="specification">{this.props.data.descendants}comments</span>
                    <span className="specification">by {this.props.data.by}</span>
                    
                </p>
                
            </div> 
         );
    } 
}
 
export default Storycontainer;
